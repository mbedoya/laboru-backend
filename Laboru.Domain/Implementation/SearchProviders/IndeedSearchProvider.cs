﻿using HtmlAgilityPack;
using Laboru.Domain.Interfaces;
using Laboru.DTO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Laboru.Domain.Implementation.SearchProviders
{
    public class IndeedSearchProvider : ISearchProvider
    {
        public string hostUrl { get; private set; } = "https://www.indeed.com";

        public IndeedSearchProvider()
        {
            
        }

        private async Task<string> GetHtmlStringFromUri(string requestUriString)
        {
            HttpClient httpClient = new HttpClient();
            string plainHtmlResults = await httpClient.GetStringAsync(requestUriString);

            return plainHtmlResults;
        }

        private HtmlNodeCollection GetHtmlNodes(string html)
        {
            const string jobNodesXPath = "//div[@data-tn-component='organicJob']";

            var doc = new HtmlDocument();
            doc.LoadHtml(html);

            var jobNodes = doc.DocumentNode.SelectNodes(jobNodesXPath);

            return jobNodes;
        }

        Func<HtmlNode, JobSearchResult> htmlNodeParser = delegate (HtmlNode node)
        {
            try
            {
                HtmlNode titleNode = node.SelectSingleNode("h2/a//@title");
                HtmlNode companyNode = node.SelectSingleNode("span[@class='company']");
                HtmlNode locationNode = node.SelectSingleNode("span[@class='location']");

                HtmlNode summaryNode = null;
                if (node.Descendants("span").Count() > 0)
                {
                    summaryNode = node.Descendants("span").Where(n => n.Attributes["class"].Value == "summary").First();
                }

                return new JobSearchResult()
                {
                    title = titleNode.InnerText,
                    description = summaryNode != null ? summaryNode.InnerText : "",
                    companyName = companyNode.InnerText,
                    location = locationNode.InnerText
                };
            }
            catch (Exception ex)
            {
                return new JobSearchResult()
                {
                    title = "Error"
                };
            }
        };

        public async Task<IEnumerable<JobSearchResult>> GetResults(string searchText)
        {
            const string keyElementToFindAtResults = "searchCount";
            const string searchUrlPath = "/jobs?q=";

            if (String.IsNullOrEmpty(searchText))
            {
                throw new Exception("Search text should not be empty");
            }

            string requestUriString = hostUrl + searchUrlPath + searchText;
            string plainHtmlResults = await GetHtmlStringFromUri(requestUriString);

            if (!plainHtmlResults.Contains(keyElementToFindAtResults))
            {
                throw new Exception("Base Element not found at provider (" + hostUrl + ") results");
            }

            var jobNodes = GetHtmlNodes(plainHtmlResults);
            var searchResults = jobNodes.Select<HtmlNode, JobSearchResult>(htmlNodeParser);

            return searchResults;
        }
    }
}
