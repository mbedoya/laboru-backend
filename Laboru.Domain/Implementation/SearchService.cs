﻿using Laboru.Domain.Implementation.SearchProviders;
using Laboru.Domain.Interfaces;
using Laboru.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboru.Domain.Implementation
{
    public class SearchService : ISearchService
    {
        public async Task<IEnumerable<JobSearchResult>> GetResults(string searchText)
        {
            IEnumerable<JobSearchResult> results = new List<JobSearchResult>();

            ISearchProvider searchProvider = new IndeedSearchProvider();
            results = await searchProvider.GetResults(searchText);

            /*
            string[] searchCriterias = searchText.Split(' ');
            foreach (var item in searchCriterias)
            {
                results.Add(new JobSearchResult() {
                    title = "Senior Developer " + item,
                    description = "We need a strong " + item + " developer to be a part of an amazing group",
                    companyName = "Emtelco",
                    startDate = DateTime.Now.AddDays(DateTime.Now.Second % 10),
                    salary = DateTime.Now.Millisecond,
                    salaryCurrency = "USD"
                });
            }*/

            return results;
        }
    }
}
