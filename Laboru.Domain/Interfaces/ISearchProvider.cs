﻿using Laboru.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboru.Domain.Interfaces
{
    public interface ISearchProvider
    {
        string hostUrl { get; }
        Task<IEnumerable<JobSearchResult>> GetResults(string searchText);
    }
}
