﻿using Laboru.Domain.Implementation;
using Laboru.Domain.Interfaces;
using Laboru.DTO;
using Laboru.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Laboru.WebApi.Controllers
{
    public class SearchController : ApiController
    {
        ISearchService searchService;

        public SearchController()
        {
            searchService = new SearchService();
        }

        // GET: api/Search
        public async Task<IEnumerable<JobSearchResult>> Get(string searchText)
        {
            return await searchService.GetResults(searchText);
        }

        // GET: api/Search/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Search
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Search/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Search/5
        public void Delete(int id)
        {
        }
    }
}
