﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Laboru.WebApi.Models
{
    public class SearchResult
    {
        public string title { get; set; }
        public string description { get; set; }
    }
}