﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboru.DTO
{
    public class JobSearchResult
    {
        public string title { get; set; }
        public string description { get; set; }
        public string companyName { get; set; }
        public string location { get; set; }
        public double salary { get; set; }
        public string salaryCurrency { get; set; }
        public DateTime? startDate { get; set; }
    }
}
