﻿using Laboru.Domain.Implementation.SearchProviders;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Laboru.Domain.Tests
{
    [TestFixture]
    public class IndeedSearchProviderTests
    {
        /*
        [Test]
        public void GetResults_ResultsDivFound_DivIsFound()
        {
            string searchText = ".Net";

            IndeedSearchProvider searchProvider = new IndeedSearchProvider();
            Assert.That( async() => await searchProvider.GetResults(searchText), Is.EqualTo(null));
        }
        */

        [Test]
        [AsyncStateMachine(typeof(Task))]
        public async Task GetResults_OneTermSent_SearchResults()
        {
            string searchText = ".Net";

            IndeedSearchProvider searchProvider = new IndeedSearchProvider();
            var result = await searchProvider.GetResults(searchText);

            Assert.AreEqual(10, result.Count());
        }
    }
}
